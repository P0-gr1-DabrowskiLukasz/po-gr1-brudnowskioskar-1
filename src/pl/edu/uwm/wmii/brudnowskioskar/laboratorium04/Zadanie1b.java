package pl.edu.uwm.wmii.brudnowskioskar.laboratorium04;

import java.util.*;


class Oski
{
    static int countSubStr(String str, String subStr)
    {

        String eq;
        int start = 0;
        int count = 0;
        int len = subStr.length();

        for(int i=0; i < str.length(); i++)
        {
            eq = str.substring(start,i+len);   // oskar
            start++;

            if(eq.equals(subStr))
            {
                count++;
            }


            if(i+len == str.length())
            {
                break;
            }


        }
        System.out.println("Ilosc wystepowania " + subStr+  " w podanym slowie: " + count);
        return count;


    }


}

public class Zadanie1b
{
    public static void main(String[] args)
    {
        String slowo;
        String podslowo;

        Scanner s = new Scanner(System.in);

        System.out.println("Wrpowadz slowo");
        slowo = s.nextLine();

        System.out.println("Wprowadz fragment do wyszukania");
        podslowo = s.nextLine();


        Oski.countSubStr(slowo, podslowo);



    }

}
