package pl.edu.uwm.wmii.brudnowskioskar.laboratorium02;

import java.util.Scanner;
import java.util.Random;

public class Zadanie2a
{
    public static void main(String[] args)
    {
        Scanner read = new Scanner(System.in);
        System.out.println("Podaj liczbe calkowita z przedzialu <1-100> :");
        int n = read.nextInt();
        while(n>100 || n<1)
        {
            System.out.println("Niepoprawny przedzial, podaj jeszcze raz:");
            n = read.nextInt();
        }
        int liczby[] = new int[n];
        int p=0;
        int np=0;
        generuj(liczby, n, -999, 999);
        p=ileParzystych(liczby);
        np=ileNieparzystych(liczby);
        wypisz(liczby);
        System.out.println("Parzyste: " + p + "  Nieparzyste: "+ np);
    }

    public static void generuj(int tab[], int n, int min, int max)
    {
        Random rnd = new Random();
        int x;
        if (min>=0)
            x=max-min+1;
        else
            x=(-1)*min+max+1;

        for (int i=0; i<n; i++)
            tab[i]=rnd.nextInt(x)+min;
    }

    public static void wypisz(int tab[])
    {
        for (int i=0; i<tab.length; i++)
        {
            System.out.print(tab[i]+"  ");
        }
        System.out.println(" ");
    }

    public static int ileNieparzystych (int tab[])
    {
        int np=0;
        for (int i=0; i<tab.length; i++)
        {
            if(tab[i]%2!=0)
                np++;
        }
        return np;
    }

    public static int ileParzystych (int tab[])
    {
        int p = 0;
        for (int i=0; i<tab.length; i++)
        {
            if(tab[i]%2==0)
                p++;
        }
        return p;
    }
}