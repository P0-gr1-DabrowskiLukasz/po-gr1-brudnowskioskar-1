package pl.edu.uwm.wmii.brudnowskioskar.laboratorium02;

import java.util.Random;
import java.util.Scanner;

public class Zadanie2g {
    public static void main(String[] args)
    {
        Scanner read = new Scanner(System.in);
        System.out.println("Podaj liczbe calkowita z przedzialu <1-100> :");
        int n = read.nextInt();
        while (n > 100 || n < 1) {
            System.out.println("Niepoprawny przedzial, podaj jeszcze raz:");
            n = read.nextInt();
        }
        Random rnd = new Random();
        int liczby[] = new int[n];

        int lewy, prawy;
        System.out.println("Lewy: ");
        lewy=read.nextInt();
        System.out.println("Prawy: ");
        prawy=read.nextInt();

        if(1<=lewy && lewy<n && 1<=prawy && prawy<n && prawy>lewy)
        {
            generuj(liczby, n, -999, 999);
            wypisz(liczby);
            odwrocFragment(liczby, lewy, prawy);
            wypisz(liczby);
        }
        else
        {
            System.out.println("Warunek dodatkowy - Lewy < Prawy");
        }
    }

    public static void generuj(int tab[], int n, int min, int max)
    {
        Random rnd = new Random();
        int x;
        if (min>=0)
            x=max-min+1;
        else
            x=(-1)*min+max+1;

        for (int i=0; i<n; i++)
            tab[i]=rnd.nextInt(x)+min;
    }

    public static void wypisz(int tab[])
    {
        for (int i=0; i<tab.length; i++)
        {
            System.out.print(tab[i]+"  ");
        }
        System.out.println(" ");
    }

    public static void odwrocFragment (int tab[] , int lewy, int prawy)
    {
        int x=0;
        int y;
        for(int i=lewy-1; i<prawy-1; i++)
        {
            y=tab[i];
            tab[i]=tab[prawy-1-x];
            tab[prawy-1-x]=y;

            if(i==(prawy-1-x))
                break;
            x++;
        }
    }
}
