package pl.edu.uwm.wmii.brudnowskioskar.laboratorium02;

import java.util.Random;
import java.util.Scanner;

public class Zadanie3 {

    public static void main(String[] args)
    {
        Scanner read = new Scanner(System.in);
        System.out.println("Podaj 3 liczby z zakresu 1-10: ");
        int m = read.nextInt();
        int n = read.nextInt();
        int k = read.nextInt();
        int[][] A = new int[m][n];
        int[][] B = new int[n][k];
        generujMacierz(A, m, n, 0, 9);
        generujMacierz(B, n, k, 0, 9);
        System.out.println("Macierz A: ");
        wyswietlMacierz(A, m, n);
        System.out.println("Macierz B: ");
        wyswietlMacierz(B, n, k);
        iloczynMacierzy(A, B, m, n, k);

    }

    public static void iloczynMacierzy(int A[][], int B[][], int m, int n, int k)
    {
        int[][] C = new int[m][k];
        int x=0;
        for (int i = 0; i < m; i++)
        {
            for (int j = 0; j < k; j++)
            {
                for (int l = 0; l < n; l++)
                {
                    x = x + (A[i][l] * B[l][j]);
                }
                C[i][j]=x;
                x=0;
            }
        }
        System.out.println("Macierz C: ");
        wyswietlMacierz(C, m, k);
    }

    public static void generujMacierz(int tab[][], int a, int b, int min, int max)
    {
        Random rnd = new Random();
        int x;
        if (min >= 0)
            x = max - min + 1;
        else
            x = (-1)*min + max + 1;
        for (int i = 0; i < a; i++)
        {
            for (int j = 0; j < b; j++)
            {
                tab[i][j] = rnd.nextInt(x) + min;
            }
        }
    }

    public static void wyswietlMacierz(int tab[][], int a, int b)
    {
        for (int i = 0; i < a; i++)
        {
            for (int j = 0; j < b; j++)
            {
                System.out.print(tab[i][j] + "   ");
            }
            System.out.println(" ");
        }
        System.out.println(" ");
    }
}