package pl.edu.uwm.wmii.brudnowskioskar.laboratorium02;

import java.util.Random;
import java.util.Scanner;

public class Zadanie1b
{
    public static void main(String[] args) {
        Scanner read = new Scanner(System.in);
        System.out.println("Podaj liczbe calkowita z przedzialu <1-100> :");
        int n = read.nextInt();
        while(n>100 || n<1)
        {
            System.out.println("Niepoprawny przedzial, podaj jeszcze raz:");
            n = read.nextInt();
        }
        Random rnd = new Random();
        int liczby[] = new int[n];
        int d=0;
        int u=0;
        int z=0;
        for(int i=0; i<n; i++)
        {
            liczby[i]=rnd.nextInt(1999)-999;
            System.out.println(liczby[i]);
            if (liczby[i]>0)
                d++;
            else if (liczby[i]<0)
                u++;
            else
                z++;
        }
        System.out.println("Dodatnie: " + d + " Ujemne: " + u + "Zerowe: " +z);
    }
}