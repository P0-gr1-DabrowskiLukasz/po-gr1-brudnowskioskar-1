package pl.edu.uwm.wmii.brudnowskioskar.laboratorium01;

import java.util.Scanner;

public class Zadanie1 {

    public void ZadanieA()
    {
        System.out.println();
        System.out.println("Zadanie 1a");
        System.out.println();

        Scanner s = new Scanner(System.in);
        System.out.println("Podaj liczbę n: ");
        int n = s.nextInt();
        int wynik = 0;
        if (n > 0)
        {
            for (int i = 1; i < n + 1; i++)
            {
                System.out.println("Podaj liczbę " + i + ". :");
                int liczby = s.nextInt();
                wynik = wynik + liczby;
            }
            System.out.println("Wynik: " + wynik);
        }
        else
        {
            System.out.println("Podana liczba nie jest naturalna.");
        }
    }

    public void ZadanieB()
    {
        System.out.println();
        System.out.println("Zadanie 1b");
        System.out.println();

        Scanner s = new Scanner(System.in);
        System.out.println("Podaj liczbę n: ");
        int n = s.nextInt();
        int wynik = 1;
        if (n > 0)
        {
            for (int i = 1; i < n + 1; i++)
            {
                System.out.println("Podaj liczbę " + i + ". :");
                int liczby = s.nextInt();
                wynik = wynik * liczby;
            }
            System.out.println("Wynik: " + wynik);
        }
        else
        {
            System.out.println("Podana liczba nie jest naturalna.");
        }
    }

    public void ZadanieC()
    {
        System.out.println();
        System.out.println("Zadanie 1c");
        System.out.println();

        Scanner s = new Scanner(System.in);
        System.out.println("Podaj liczbę n: ");
        int n = s.nextInt();
        int wynik = 0;
        if (n > 0)
        {
            for (int i = 1; i < n + 1; i++)
            {
                System.out.println("Podaj liczbę " + i + ". :");
                int liczby = s.nextInt();
                wynik = wynik + Math.abs(liczby);
            }
            System.out.println("Wynik: " + wynik);
        }
        else
        {
            System.out.println("Podana liczba nie jest naturalna.");
        }
    }

    public void ZadanieD()
    {
        System.out.println();
        System.out.println("Zadanie 1d");
        System.out.println();

        Scanner s = new Scanner(System.in);
        System.out.println("Podaj liczbę n: ");
        int n = s.nextInt();
        double wynik = 0;
        if (n > 0)
        {
            for (int i = 1; i < n + 1; i++)
            {
                System.out.println("Podaj liczbę " + i + ". :");
                double liczby = s.nextInt();
                wynik = wynik + Math.sqrt(Math.abs(liczby));
            }
            System.out.println("Wynik: " + wynik);
        }
        else
        {
            System.out.println("Podana liczba nie jest naturalna.");
        }
    }


    public void ZadanieE()
    {
        System.out.println();
        System.out.println("Zadanie 1e");
        System.out.println();

        Scanner s = new Scanner(System.in);
        System.out.println("Podaj liczbę n: ");
        int n = s.nextInt();
        int wynik = 1;
        if (n > 0)
        {
            for (int i = 1; i < n + 1; i++)
            {
                System.out.println("Podaj liczbę " + i + ". :");
                int liczby = s.nextInt();
                wynik = wynik * Math.abs(liczby);
            }
            System.out.println("Wynik: " + wynik);
        }
        else
        {
            System.out.println("Podana liczba nie jest naturalna.");
        }
    }

    public void ZadanieF()
    {
        System.out.println();
        System.out.println("Zadanie 1f");
        System.out.println();

        Scanner s = new Scanner(System.in);
        System.out.println("Podaj liczbę n: ");
        int n = s.nextInt();
        double wynik = 0;
        if (n > 0)
        {
            for (int i = 1; i < n + 1; i++)
            {
                System.out.println("Podaj liczbę " + i + ". :");
                double liczby = s.nextInt();
                wynik = wynik + Math.pow(liczby, 2);
            }
            System.out.println("Wynik: " + wynik);
        }
        else
        {
            System.out.println("Podana liczba nie jest naturalna.");
        }
    }

    public void ZadanieG()
    {
        System.out.println();
        System.out.println("Zadanie 1g");
        System.out.println();

        Scanner s = new Scanner(System.in);
        System.out.println("Podaj liczbę n: ");
        int n = s.nextInt();
        int suma = 0;
        int iloczyn = 0;
        if (n > 0)
        {
            for (int i = 1; i < n + 1; i++)
            {
                System.out.println("Podaj liczbę " + i + ". :");
                int liczby = s.nextInt();
                suma = suma + liczby;
                iloczyn = iloczyn * liczby;
            }
            System.out.println("Suma wynosi: " + suma);
            System.out.println("Iloczyn wynosi: " + iloczyn);
        }
        else
        {
            System.out.println("Podana liczba nie jest naturalna.");
        }
    }


    public void ZadanieH()
    {
        System.out.println();
        System.out.println("Zadanie 1h");
        System.out.println();

        Scanner s = new Scanner(System.in);
        System.out.println("Podaj liczbę n: ");
        int n = s.nextInt();
        double wynik = 0;
        if (n > 0)
        {
            for (int i = 1; i < n + 1; i++)
            {
                System.out.println("Podaj liczbę " + i + ". :");
                int liczby = s.nextInt();
                wynik = wynik + Math.pow(-1,i+1)*liczby;
            }
            System.out.println("Wynik: " + wynik);
        }
        else
        {
            System.out.println("Podana liczba nie jest naturalna.");
        }
    }

    public void ZadanieI()
    {
        System.out.println();
        System.out.println("Zadanie 1i");
        System.out.println();

        Scanner s = new Scanner(System.in);
        System.out.println("Podaj liczbę n: ");
        int n = s.nextInt();
        double wynik = 0;
        int silnia = 1;
        if (n > 0)
        {
            for (int i = 1; i < n + 1; i++)
            {
                silnia=silnia*i;
                System.out.println("Podaj liczbę " + i + ". :");
                int liczby = s.nextInt();
                wynik = wynik + (Math.pow(-1,i)*liczby)/(silnia);
            }
            System.out.println("Wynik: " + wynik);
        }
        else
        {
            System.out.println("Podana liczba nie jest naturalna.");
        }
    }


    public static void main(String[] args) {

        Zadanie1 i = new Zadanie1();
        i.ZadanieA();
        i.ZadanieB();
        i.ZadanieC();
        i.ZadanieD();
        i.ZadanieE();
        i.ZadanieF();
        i.ZadanieG();
        i.ZadanieH();
        i.ZadanieI();

    }
}