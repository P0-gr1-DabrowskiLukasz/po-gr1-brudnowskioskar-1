package pl.edu.uwm.wmii.brudnowskioskar.laboratorium01;

import java.util.Scanner;

public class Zadanie2
{
    public void Zadanie2A()
    {
        System.out.println();
        System.out.println("Zadanie 2a");
        System.out.println();

        Scanner s = new Scanner(System.in);
        System.out.println("Podaj liczbę n: ");
        int n = s.nextInt();
        int wynik = 0;
        if (n > 0)
        {
            for (int i = 1; i < n + 1; i++)
            {
                System.out.println("Podaj liczbę " + i + ". :");
                int liczby = s.nextInt();
                if(liczby%2==1)
                {
                    wynik++;
                }
            }
            System.out.println("Wynik: " + wynik);
        }
        else
        {
            System.out.println("Podana liczba nie jest naturalna.");
        }
    }

    public void Zadanie2B()
    {
        System.out.println();
        System.out.println("Zadanie 2b");
        System.out.println();

        Scanner s = new Scanner(System.in);
        System.out.println("Podaj liczbę n: ");
        int n = s.nextInt();
        int wynik = 0;
        if (n > 0)
        {
            for (int i = 1; i < n + 1; i++)
            {
                System.out.println("Podaj liczbę " + i + ". :");
                int liczby = s.nextInt();
                if(liczby%3==0 && liczby%5!=0)
                {
                    wynik++;
                }
            }
            System.out.println("Wynik: " + wynik);
        }
        else
        {
            System.out.println("Podana liczba nie jest naturalna.");
        }
    }

    public void Zadanie2C()
    {
        System.out.println();
        System.out.println("Zadanie 2c");
        System.out.println();

        Scanner s = new Scanner(System.in);
        System.out.println("Podaj liczbę n: ");
        int n = s.nextInt();
        int wynik = 0;
        double x;
        if (n > 0)
        {
            for (int i = 1; i < n + 1; i++)
            {
                System.out.println("Podaj liczbę " + i + ". :");
                int liczby = s.nextInt();
                x=Math.sqrt((double) liczby);
                if(x-(int)x==0 &&(int)x%2==0)
                {
                    wynik++;
                }
            }
            System.out.println("Wynik: " + wynik);
        }
        else
        {
            System.out.println("Podana liczba nie jest naturalna.");
        }
    }

    public void Zadanie2D()
    {
        System.out.println();
        System.out.println("Zadanie 2d");
        System.out.println();

        Scanner s = new Scanner(System.in);
        System.out.println("Podaj liczbę n: ");
        int n = s.nextInt();
        int wynik = 0;
        int liczby2;
        if (n > 0)
        {
            System.out.println("Podaj liczby: ");
            int liczby1=s.nextInt();
            int liczby=s.nextInt();
            for(int i=3; i<=n; i++)
            {
                liczby2=liczby1;
                liczby1=liczby;
                System.out.println("Podaj liczbę " + i + ". :");
                liczby=s.nextInt();
                if(liczby1<((liczby2+liczby)/2))
                {
                    wynik++;
                }
            }


            System.out.println("Wynik: " + wynik);
        }
        else
        {
            System.out.println("Podana liczba nie jest naturalna.");
        }
    }

    public void Zadanie2E()
    {
        System.out.println();
        System.out.println("Zadanie 2e");
        System.out.println();

        Scanner s = new Scanner(System.in);
        System.out.println("Podaj liczbę n: ");
        int n = s.nextInt();
        int wynik=0;
        int potega=1;
        int silnia=1;
        if (n > 0)
        {
            for (int i = 1; i < n+1; i++)
            {
                potega=potega*2;
                silnia=silnia*i;
                System.out.println("Podaj liczbę " + i + ". :");
                int liczby = s.nextInt();
                if (liczby>potega && liczby<silnia)
                {
                    wynik++;
                }
            }
            System.out.println("Wynik: " + wynik);
        }
        else
        {
            System.out.println("Podana liczba nie jest naturalna.");
        }
    }

    public void Zadanie2F()
    {
        System.out.println();
        System.out.println("Zadanie 2f");
        System.out.println();

        Scanner s = new Scanner(System.in);
        System.out.println("Podaj liczbę n: ");
        int n = s.nextInt();
        int wynik=0;
        if (n > 0)
        {
            for (int i = 1; i < n+1; i++)
            {
                System.out.println("Podaj liczbę " + i + ". :");
                int liczby = s.nextInt();
                if (i%2==1 && liczby%2==0)
                {
                    wynik++;
                }
            }
            System.out.println("Wynik: " + wynik);
        }
        else
        {
            System.out.println("Podana liczba nie jest naturalna.");
        }
    }

    public void Zadanie2G()
    {
        System.out.println();
        System.out.println("Zadanie 2g");
        System.out.println();

        Scanner s = new Scanner(System.in);
        System.out.println("Podaj liczbę n: ");
        int n = s.nextInt();
        int wynik=0;
        if (n > 0)
        {
            for (int i = 1; i < n+1; i++)
            {
                System.out.println("Podaj liczbę " + i + ". :");
                int liczby = s.nextInt();
                if (liczby<0)
                {
                    liczby=liczby*-1;
                }
                if(liczby<i*i)
                {
                    wynik++;
                }
            }
            System.out.println("Wynik: " + wynik);
        }
        else
        {
            System.out.println("Podana liczba nie jest naturalna.");
        }
    }

    public void Zadanie2H()
    {
        System.out.println();
        System.out.println("Zadanie 2h");
        System.out.println();

        Scanner s = new Scanner(System.in);
        System.out.println("Podaj liczbę n: ");
        int n = s.nextInt();
        int wynik=0;
        double x;
        if (n > 0)
        {
            for (int i = 1; i < n+1; i++)
            {
                System.out.println("Podaj liczbę " + i + ". :");
                int liczby = s.nextInt();
                x=Math.sqrt((double) liczby);
                if(x-(int)x==0 && (int)x%2==0)
                {
                    wynik++;
                }
            }
            System.out.println("Wynik: " + wynik);
        }
        else
        {
            System.out.println("Podana liczba nie jest naturalna.");
        }
    }

    public void Zadanie22()
    {
        System.out.println();
        System.out.println("Zadanie 2.2");
        System.out.println();

        Scanner s = new Scanner(System.in);
        System.out.println("Podaj liczbę n: ");
        int n = s.nextInt();
        int wynik=0;
        if (n > 0)
        {
            for (int i = 1; i < n+1; i++)
            {
                System.out.println("Podaj liczbę " + i + ". :");
                int liczby = s.nextInt();
                if(liczby>0)
                {
                    wynik=wynik+2*liczby;
                }
                else
                {
                    wynik=wynik+liczby;
                }
            }
            System.out.println("Wynik: " + wynik);
        }
        else
        {
            System.out.println("Podana liczba nie jest naturalna.");
        }
    }

    public void Zadanie23()
    {
        System.out.println();
        System.out.println("Zadanie 2.3");
        System.out.println();

        Scanner s = new Scanner(System.in);
        System.out.println("Podaj liczbę n: ");
        int n = s.nextInt();
        int dodatnie=0;
        int ujemne=0;
        int zera=0;
        if (n > 0)
        {
            for (int i = 1; i < n+1; i++)
            {
                System.out.println("Podaj liczbę " + i + ". :");
                int liczby = s.nextInt();
                if(liczby>0)
                {
                    dodatnie=dodatnie+1;
                }
                else if(liczby<0)
                {
                    ujemne=ujemne+1;
                }
                else
                {
                    zera=zera+1;
                }
            }
            System.out.println("Dodatnie: " + dodatnie);
            System.out.println("Ujemne: " + ujemne);
            System.out.println("Zera: " + zera);
        }
        else
        {
            System.out.println("Podana liczba nie jest naturalna.");
        }
    }

    public void Zadanie24()
    {
        System.out.println();
        System.out.println("Zadanie 2.4");
        System.out.println();

        Scanner s = new Scanner(System.in);
        System.out.println("Podaj liczbę n: ");
        int n = s.nextInt();
        int najm=0;
        int najw=0;
        if (n > 0)
        {
            for (int i = 1; i < n+1; i++)
            {
                System.out.println("Podaj liczbę " + i + ". :");
                int liczby = s.nextInt();
                if(i==1)
                {
                    najm=liczby;
                    najw=liczby;
                }
                else if(liczby>najw)
                {
                    najw=liczby;
                }
                else if(liczby<najm)
                {
                    najm=liczby;
                }
            }
            System.out.println("Najmniejsza: " + najm);
            System.out.println("Najwieksza: " + najw);
        }
        else
        {
            System.out.println("Podana liczba nie jest naturalna.");
        }
    }

    public static void main(String[] args)
    {
        Zadanie2 i = new Zadanie2();
        i.Zadanie2A();
        i.Zadanie2B();
        i.Zadanie2C();
        i.Zadanie2D();
        i.Zadanie2E();
        i.Zadanie2F();
        i.Zadanie2G();
        i.Zadanie2H();
        i.Zadanie22();
        i.Zadanie23();
        i.Zadanie24();

    }
}