package pl.edu.uwm.wmii.brudnowskioskar.laboratorium00;

public class Zadanie11
{
    public static void main(String[] args)
    {
        System.out.println("Znam ludzi, którym w sercach zgasło,");
        System.out.println("lecz mówią:ciepło na i jasno,");
        System.out.println("i bardzo kłamią, gdy się śmieją");
        System.out.println("Wiem jak ułożyć rysy twarzy ");
        System.out.println("by smutku nikt nie zauważył.");
    }
}
